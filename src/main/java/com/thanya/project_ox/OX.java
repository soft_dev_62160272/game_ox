/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.project_ox;

import java.util.Scanner;

/**
 *
 * @author Thanyarak Namwong
 */
public class OX {

	public static void main(String[] args) {

		Scanner kb = new Scanner(System.in);
		char[][] board = new char[4][4];
		char ps1 = 'X';
		char ps2 = 'O';

		board[1][0] = '1';
		board[2][0] = '2';
		board[3][0] = '3';
		board[0][1] = '1';
		board[0][2] = '2';
		board[0][3] = '3';

		System.out.println("Welcome to OX Game");
		for (int i = 1; i < 4; i++) {
			for (int j = 1; j < 4; j++) {
				board[i][j] = '-';
			}

		}

		boolean player = true;

		boolean gameEnd = false;

		while (!gameEnd) { // End loop when gameEnd is true

			printboard(board);

			char symbol = ' ';
			// if player is true
			if (player) {
				symbol = 'X';
			} else {
			// if player is false
				symbol = 'O';
			}

			if (player) {
				System.out.println(ps1 + " turn");
			} else {
				System.out.println(ps2 + " turn");
			}

			int row = 0;
			int col = 0;

			while (true) {
				System.out.println("Please input Row Col : ");
				row = kb.nextInt();
				col = kb.nextInt();

				if (row < 0 || col < 0 || row > 3 || col > 3) {
					System.out.println("No position please try again");
				} else if (board[row][col] != '-') {
					System.out.println("position are not already");
				} else {
					break;
				}
			}
			// replace row and column == X and O
			board[row][col] = symbol;

			if (checkwin(board) == 'X') { // check position is X
				printboard(board);
				System.out.println("Player " + ps1 + " Win....");
				System.out.println("Bye bye ....");
				gameEnd = true;
			} else if (checkwin(board) == 'O') { // check position is O
				printboard(board);
				System.out.println("Player " + ps2 + " Win....");
				System.out.println("Bye bye ....");
				gameEnd = true;
			} else {
				if (checkdraw(board)) { // check position is draw
					printboard(board);
					System.out.println("Draw....");
					System.out.println("Bye bye ....");
					gameEnd = true;
				} else {
					player = !player; // true = false
				}
			}
		}

	}

	public static void printboard(char[][] board) { // view board
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(board[i][j] + " ");
			}

			System.out.println();

		}

	}

	public static char checkwin(char[][] board) {
		// check row
		for (int i = 1; i < 4; i++) {
			if (board[i][1] == board[i][2] && board[i][2] == board[i][3] && board[i][1] != '-') {
				return board[i][1];
			}
		}

		// check column
		for (int j = 1; j < 4; j++) {
			if (board[1][j] == board[2][j] && board[2][j] == board[3][j] && board[1][j] != '-') {
				return board[1][j];
			}
		}

		// check diagonal
		if (board[1][1] == board[2][2] && board[2][2] == board[3][3] && board[1][1] != '-') {
			return board[1][1];
		}
		if (board[3][1] == board[2][2] && board[2][2] == board[1][3] && board[3][1] != '-') {
			return board[3][1];
		}

		// return to player X to O and O to X
		return '-';

	}

	public static boolean checkdraw(char[][] board) {
		// return to player X and O
		for (int i = 1; i < 4; i++) {
			for (int j = 1; j < 4; j++) {
				if (board[i][j] == '-') {
					return false;
				}
			}
		}
		// check board is draw
		return true;

	}

}

